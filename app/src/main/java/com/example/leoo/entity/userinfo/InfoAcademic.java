package com.example.leoo.entity.userinfo;

public class InfoAcademic {
    private String rollNumber;
    private String campus;
    private String memberCode;
    private String mode;
    private String status;
    private int currentTerm;
    private String major;
    private String curriculum;

    public InfoAcademic(String rollNumber, String campus, String memberCode, String mode, String status, int currentTerm, String major, String curriculum) {
        this.rollNumber = rollNumber;
        this.campus = campus;
        this.memberCode = memberCode;
        this.mode = mode;
        this.status = status;
        this.currentTerm = currentTerm;
        this.major = major;
        this.curriculum = curriculum;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCurrentTerm() {
        return currentTerm;
    }

    public void setCurrentTerm(int currentTerm) {
        this.currentTerm = currentTerm;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }
}
