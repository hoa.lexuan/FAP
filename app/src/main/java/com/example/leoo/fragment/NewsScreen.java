package com.example.leoo.fragment;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.example.leoo.entity.others.News;
import com.example.leoo.fap.R;
import com.example.leoo.subactivity.NewsDetail;

import java.util.ArrayList;

public class NewsScreen extends Fragment {

    private String[] number = {"0988222999","0988933333","0182882883","0935727882","0928873882","01667887333"};

    ArrayAdapter arrayAdapter;

    ListView listView ;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("News");
        View view = inflater.inflate(R.layout.fragment_news_screen, container, false);
        listView = (ListView)view.findViewById(R.id.gridViewNews);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), " " + "Search", Toast.LENGTH_SHORT).show();
            }
        });

        final ArrayList<News> listNews = getListData();

        ArrayAdapter arrayAdapter = new ArrayAdapter(
                getActivity(),
                android.R.layout.simple_list_item_1,
                listNews
        );

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //position: return position when click on listview

                Bundle bundle = new Bundle();
                bundle.putInt("id", listNews.get(position).getId());
                bundle.putString("name", listNews.get(position).getNameNews());
                bundle.putString("code", listNews.get(position).getDatePubliced());

                Intent intent = new Intent(getActivity(), NewsDetail.class);
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        return view;
    }





    private ArrayList<News> getListData() {
        ArrayList<News> list = new ArrayList<>();
        News vietnam = new News(1, "vn", "Vietnam");
        News usa = new News(2, "us", "United States");
        News russia = new News(3, "ru", "Russia");
        News australia = new News(4, "au", "Australia");
        News japan = new News(5, "jp", "Japan");
        News vietnam1 = new News(6, "vn", "Vietnam");
        News usa1 = new News(7, "us", "United States");
        News russia1 = new News(8, "ru", "Russia");
        News australia1 = new News(9, "au", "Australia");
        News japan1 = new News(10, "jp", "Japan");
        News vietnam2 = new News(11, "vn", "Vietnam");
        News usa2 = new News(12, "us", "United States");
        News russia2 = new News(13, "ru", "Russia");
        News australia2 = new News(14, "au", "Australia");
        News japan2 = new News(15, "jp", "Japan");
        News vietnam3 = new News(16, "vn", "Vietnam");
        News usa3 = new News(17, "us", "United States");
        News russia3 = new News(18, "ru", "Russia");
        News australia3 = new News(19, "au", "Australia");
        News japan3 = new News(20, "jp", "Japan");

        list.add(vietnam);
        list.add(usa);
        list.add(russia);
        list.add(australia);
        list.add(japan);

        list.add(vietnam1);
        list.add(usa1);
        list.add(russia1);
        list.add(australia1);
        list.add(japan1);

        list.add(vietnam2);
        list.add(usa2);
        list.add(russia2);
        list.add(australia2);
        list.add(japan2);

        list.add(vietnam3);
        list.add(usa3);
        list.add(russia3);
        list.add(australia3);
        list.add(japan3);

        return list;
    }

    }
