package com.example.leoo.entity.others;

public class News {

    private int id;
    private String nameNews;
    private String datePubliced;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameNews() {
        return nameNews;
    }

    public void setNameNews(String nameNews) {
        this.nameNews = nameNews;
    }

    public String getDatePubliced() {
        return datePubliced;
    }

    public void setDatePubliced(String datePubliced) {
        this.datePubliced = datePubliced;
    }

    public News(int id, String nameNews, String datePubliced)  {
        this.id= id;
        this.nameNews= nameNews;
        this.datePubliced = datePubliced;
    }

    @Override
    public String toString() {
        return id + " - " + nameNews + " - " + datePubliced;
    }
}
