package com.example.leoo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leoo.fap.LoginScreen;
import com.example.leoo.fap.R;
import com.example.leoo.fragment.FeedbackScreen;
import com.example.leoo.fragment.InformationAccessScreen;
import com.example.leoo.fragment.NewsScreen;
import com.example.leoo.fragment.RegistrationScreen;
import com.example.leoo.fragment.ReportsScreen;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

public class HomeScreens extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView textView, emailView;
    ImageView imageView;
    Button btnLogout;
    String image;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screens);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//            }
//        });

        DrawerLayout drawer = findViewById(R.id.homeScreen);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //handle information of user
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(HomeScreens.this);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.invalidate();

        View headerView = navigationView.getHeaderView(0);

        emailView = headerView.findViewById(R.id.emailView);
        textView = headerView.findViewById(R.id.textView);
        imageView = headerView.findViewById(R.id.imageView);

        //set simple profile into app
        //avatar, name, email
        if (acct != null) {
            emailView.setText(acct.getEmail());
            textView.setText(acct.getDisplayName());
            Picasso.get().load(acct.getPhotoUrl()).into(imageView);
        }
    }

    //make login screen display one time
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    //handle action of top task bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.news) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new NewsScreen()).commit();
        } else if (id == R.id.registration) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new RegistrationScreen()).commit();
        } else if (id == R.id.information) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new InformationAccessScreen()).commit();
        } else if (id == R.id.report) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new ReportsScreen()).commit();
        } else if (id == R.id.feedback) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new FeedbackScreen()).commit();
        } else if (id == R.id.nav_account) {
            Intent intent = new Intent(this, Account.class);
            startActivity(intent);
        }else if (id == R.id.nav_logout) {
            signOut();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.homeScreen);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // ...
                        Toast.makeText(getApplicationContext(),"Logged Out",Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getApplicationContext(),LoginScreen.class);
                        startActivity(i);
                    }
                });
    }
}
