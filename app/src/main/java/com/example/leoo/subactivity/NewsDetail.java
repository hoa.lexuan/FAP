package com.example.leoo.subactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.leoo.fap.R;

public class NewsDetail extends AppCompatActivity {

    TextView a, b, c;
    String id;
    String name;
    String code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        a = findViewById(R.id.textView3);
        b = findViewById(R.id.textView9);
        c = findViewById(R.id.textView10);


        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            id = String.valueOf(bundle.getInt("id"));
            name = bundle.getString("name");
            code = bundle.getString("code");
        }
        System.out.println("check");
        a.setText(id);
        b.setText(name);
        c.setText(code);
        System.out.println(name);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
