package com.example.leoo.fap;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.leoo.activity.HomeScreens;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import gr.net.maroulis.library.EasySplashScreen;

public class LoadingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);

        EasySplashScreen config = new EasySplashScreen(LoadingScreen.this)
                .withFullScreen()
                .withSplashTimeOut(5000)
                .withBackgroundColor(Color.parseColor("#074E72"))
                .withHeaderText("Leoo")
                .withBeforeLogoText("Hey Hey");

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(LoadingScreen.this);
        if(acct != null) {
            config.withTargetActivity(HomeScreens.class);
        }else if(acct == null) {
            PackageManager pm = getPackageManager();
            pm.setComponentEnabledSetting(new ComponentName(this, LoginScreen.class),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

            config.withTargetActivity(LoginScreen.class);
        }

        config.getHeaderTextView().setTextColor(Color.BLACK);

        View view = config.create();
        setContentView(view);

    }
}
